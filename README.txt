Andrei Epure's homework for Optravis

PROJECT STRUCTURE
- library
--> /src/main - implementation of generating random list and computing intersection
--> /src/test - unit tests
- web-ui
--> the Spring Boot Web App for the UI
--> /src/main - the spring boot app (see the Controllers package)
--> /src/test - unit tests
--> /src/main/resources/static - static files
---> /index.html
---> /script/submit.js - the client side logic
---> /css/style.css - the custom CSS (I also used Boostrap)

HOW TO RUN
From the root folder run:
> mvn install
Now the library and web-app jars are built.
> cd web-ui
> mvn spring-boot:run

Open your browser to http://localhost:48080
Note: Firefox seems to have problems interpreting the HTML5 form validation for the number input, min and max. Tested with IE and (curiously) it works better.

TASK
Java Application
The application should provide a user interface which can be used to enter the following parameters:
�  Size of list A
�  Size of list B
�  Choose which list is put into a HashSet, which one is iterated over
�  A Run-button to start the computation
�  An output fields to show the size of the result-set
�  A second output field to show the time it took to run the algorithm (generating the two input-lists
should not be counted)
If the Run-button is pressed, the two lists are populated with random numbers. Based on the user input, one
of the lists is put into a HashSet, the other one is iterated over to test for each element if it is contained in the
HashSet. Matching elements are put into the result set.