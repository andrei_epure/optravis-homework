package com.optravis.homework.andrei.web.controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class ListConfigTest {
	private static final int LIST_A_SIZE = 10;
	private static final int LIST_B_SIZE = 42;

	@Test
	public void isValidWithNegativeSizeReturnFalse() {
		ListConfig negativeFirst = new ListConfig(-1, LIST_B_SIZE, HashSetChoice.FIRST_LIST);
		ListConfig negativeSecond = new ListConfig(LIST_A_SIZE, -1, HashSetChoice.FIRST_LIST);

		assertFalse("not valid if first list is negative", negativeFirst.isValid());
		assertFalse("not valid if second list is negative", negativeSecond.isValid());
	}

	@Test
	public void isValidWithZeroSizeReturnFalse() {
		ListConfig zeroFirst = new ListConfig(0, LIST_B_SIZE, HashSetChoice.FIRST_LIST);
		ListConfig zeroSecond = new ListConfig(LIST_A_SIZE, 0, HashSetChoice.FIRST_LIST);

		assertFalse("not valid if first list is zero", zeroFirst.isValid());
		assertFalse("not valid if second list is zero", zeroSecond.isValid());
	}

	@Test
	public void isValidWithNullChoiceReturnsFalse() {
		ListConfig nullChoice = new ListConfig(LIST_A_SIZE, LIST_B_SIZE, null);

		assertFalse("not valid if choice is null", nullChoice.isValid());
	}

	public void isValidWithListConfig() {
		ListConfig firstChoice = new ListConfig(LIST_A_SIZE, LIST_B_SIZE, HashSetChoice.FIRST_LIST);
		ListConfig secondChoice = new ListConfig(LIST_A_SIZE, LIST_B_SIZE, HashSetChoice.FIRST_LIST);

		assertTrue("valid is true for valid ListConfig with first choice", firstChoice.isValid());
		assertTrue("valid is true for valid ListConfig with second choice", secondChoice.isValid());
	}

	@Test
	public void getSetListSizeWithFirstChoiceReturnsFirst() {
		ListConfig config = new ListConfig(LIST_A_SIZE, LIST_B_SIZE, HashSetChoice.FIRST_LIST);

		assertEquals(LIST_A_SIZE, config.getSetListSize());
	}

	@Test
	public void getSetListSizeWithSecondListChoiceReturnsSecond() {
		ListConfig config = new ListConfig(LIST_A_SIZE, LIST_B_SIZE, HashSetChoice.SECOND_LIST);

		assertEquals(LIST_B_SIZE, config.getSetListSize());
	}

	@Test
	public void getIterateListSizeWithFirstChoiceReturnsSecond() {
		ListConfig config = new ListConfig(LIST_A_SIZE, LIST_B_SIZE, HashSetChoice.FIRST_LIST);

		// if list A gets in HashSet, list B is iterated
		assertEquals(LIST_B_SIZE, config.getIterateListSize());
	}

	@Test
	public void getIterateListSizeWithSecondChoiceReturnsFirst() {
		ListConfig config = new ListConfig(LIST_A_SIZE, LIST_B_SIZE, HashSetChoice.SECOND_LIST);

		// if list B gets in HashSet, list A is iterated
		assertEquals(LIST_A_SIZE, config.getIterateListSize());
	}
}
