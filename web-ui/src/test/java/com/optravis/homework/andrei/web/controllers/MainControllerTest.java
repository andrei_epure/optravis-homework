package com.optravis.homework.andrei.web.controllers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.Test;
import org.springframework.http.ResponseEntity;

import com.optravis.homework.andrei.library.HomeworkResult;
import com.optravis.homework.andrei.library.HomeworkRunner;

public class MainControllerTest {
	@Test(expected = IllegalArgumentException.class)
	public void mainControllerWithNullRunnerThrows() {
		ExecutorService mockPool = mock(ExecutorService.class);
		MainController controller = new MainController(null, mockPool);
	}

	@Test(expected = IllegalArgumentException.class)
	public void mainControllerWithNullPoolThrows() {
		HomeworkRunner mockRunner = mock(HomeworkRunner.class);
		MainController controller = new MainController(mockRunner, null);
	}

	@Test
	public void mainControllerWithNegativeFirstListSizeReturns415() {
		HomeworkRunner mockRunner = mock(HomeworkRunner.class);
		ExecutorService mockPool = mock(ExecutorService.class);
		MainController controller = new MainController(mockRunner, mockPool);

		ResponseEntity<HomeworkResult> result = controller.compute(new ListConfig(-1, 1, HashSetChoice.FIRST_LIST));

		assertEquals(415, result.getStatusCodeValue());
	}

	@Test
	public void mainControllerWithNegativeSecondListSizeReturns415() {
		HomeworkRunner mockRunner = mock(HomeworkRunner.class);
		ExecutorService mockPool = mock(ExecutorService.class);
		MainController controller = new MainController(mockRunner, mockPool);

		ResponseEntity<HomeworkResult> result = controller.compute(new ListConfig(1, -1, HashSetChoice.FIRST_LIST));

		assertEquals(415, result.getStatusCodeValue());
	}

	@Test
	public void mainControllerWithNullHashSetChoice() {
		HomeworkRunner mockRunner = mock(HomeworkRunner.class);
		ExecutorService mockPool = mock(ExecutorService.class);
		MainController controller = new MainController(mockRunner, mockPool);

		ResponseEntity<HomeworkResult> result = controller.compute(new ListConfig(1, 1, null));

		assertEquals(415, result.getStatusCodeValue());
	}

	@Test
	public void mainControllerWithGoodInputReturnsResult() {
		HomeworkRunner mockRunner = mock(HomeworkRunner.class);
		// not ideal. guess a HomeworkResult interface would have been better, but late
		// now and I want to sleep
		when(mockRunner.timeIntersection(isA(int.class), isA(int.class)))
				.thenReturn(new HomeworkResult(Instant.now(), Instant.now(), 10));
		ExecutorService mockPool = Executors.newFixedThreadPool(2);
		MainController controller = new MainController(mockRunner, mockPool);

		ResponseEntity<HomeworkResult> result = controller.compute(new ListConfig(1, 1, HashSetChoice.FIRST_LIST));

		assertEquals(200, result.getStatusCodeValue());
	}
}