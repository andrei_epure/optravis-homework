window.onload = function () {
	// jquery ids for divs
	var LOADING = "#loading";
	var RESULT_AREA = "#result";
	var ERROR_AREA = "#error";

	// if waiting for result (already sent a request), don't allow other
	// requests to be made
	var waitingForResult = false;

	// id is jquery id
	function hide(id) {
		$(id).addClass("hidden");
	}
	// id is jquery id
	function show(id) {
		$(id).removeClass("hidden");
	}

	function setResult(data) {
		$("#duration").text(data.duration);
		$("#result-size").text(data.size);

		hide(LOADING);
		hide(ERROR_AREA);
		show(RESULT_AREA);
		waitingForResult = false;
	}

	function setError(errorText) {
		$("#error-message").text(errorText);

		hide(LOADING);
		show(ERROR_AREA);
		waitingForResult = false;
	}

	$("form").submit(function(e) {
		if (waitingForResult === true) {
			console.log("user does not have patience");

			$("#error-message").text("Please be patient until current computation ends");
			show(ERROR_AREA);

			return;
		}

		show(LOADING);
		hide(ERROR_AREA);
		hide(RESULT_AREA);

		waitingForResult = true;

		$.ajax({
			type: "POST",
			url: "compute",
			data: $("#form").serialize(), // serializes the form's elements.
			success: function(data) {
				console.log(data);
				console.log("duration=" + data.duration);
				console.log("size=" + data.size);

				setResult(data);
			},
			error: function(xhr, status, text) {
				var errorText = "Unknown error occurred";
				console.log("got error " + xhr.status);

				switch (xhr.status) {
				case 406:
					errorText = "Malformed input";
					break;
				case 408:
					errorText = "Server timeout was exceeded for computation";
					break;
				case 415:
					errorText = "Input out of range";
					break;
				case 500:
					errorText = "Internal server error";
					break;
				}

				setError("Error occurred: " + errorText);
			}
		});
	});
}