package com.optravis.homework.andrei.web.controllers;

/**
 * Server payload.
 * 
 * Spring does the parsing and will return HTTP code 406 if the parse fails,
 * so initial validation is done at parse time.
 */
public class ListConfig {
	private int firstListSize;
	private int secondListSize;
	private HashSetChoice listChoice;
	
	public ListConfig() {
		// needed by Spring when getting the data from the client
	}

	// this is used for tests, no validation
	public ListConfig(int firstListSize, int secondListSize, HashSetChoice listChoice) {
		this.firstListSize = firstListSize;
		this.secondListSize = secondListSize;
		this.listChoice = listChoice;
	}

	// not allowed to have negative or 0 size for lists
	public boolean isValid() {
		return firstListSize > 0 && secondListSize > 0 && listChoice != null;
	}

	public int getSetListSize() {
		return listChoice == HashSetChoice.FIRST_LIST ? firstListSize : secondListSize;
	}

	public int getIterateListSize() {
		return listChoice == HashSetChoice.FIRST_LIST ? secondListSize : firstListSize;
	}

	// needed by Spring
	public void setFirstListSize(int firstListSize) { this.firstListSize = firstListSize; } 
	public void setSecondListSize(int secondListSize) { this.secondListSize = secondListSize; }
	public void setListChoice(HashSetChoice listChoice) { this.listChoice = listChoice; }
}