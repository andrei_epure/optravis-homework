package com.optravis.homework.andrei.web;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.optravis.homework.andrei.library.HashSetIntersectionComputer;
import com.optravis.homework.andrei.library.HomeworkRunner;
import com.optravis.homework.andrei.library.RandomListGenerator;
import com.optravis.homework.andrei.library.SimpleHomeworkRunner;

@SpringBootApplication
public class WebUiApplication {
	private static final int POOL_SIZE = 10;

	public static void main(String[] args) {
		SpringApplication.run(WebUiApplication.class, args);
	}

	@Bean
	public HomeworkRunner homeworkRunner() {
		return new SimpleHomeworkRunner(
				new RandomListGenerator(),
				new HashSetIntersectionComputer());
	}

	@Bean
	public ExecutorService executorService() {
		return Executors.newFixedThreadPool(POOL_SIZE);
	}
}