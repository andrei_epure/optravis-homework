package com.optravis.homework.andrei.web.controllers;

/**
 * Which list will be stored in the HashSet.
 */
public enum HashSetChoice {
	FIRST_LIST,
	SECOND_LIST
}