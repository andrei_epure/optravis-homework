package com.optravis.homework.andrei.web.controllers;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.optravis.homework.andrei.library.HomeworkResult;
import com.optravis.homework.andrei.library.HomeworkRunner;

@Controller
@RequestMapping("/")
public class MainController {
	private final static Logger LOGGER = LoggerFactory.getLogger(MainController.class);
	private final static int TIMEOUT_SECONDS = 5 * 60;

	private final HomeworkRunner runner;
	private final ExecutorService pool;

	MainController(HomeworkRunner homeworkRunner, ExecutorService pool) {
		if (homeworkRunner == null) {
			throw new IllegalArgumentException("homeworkRunner cannot be null");
		}
		if (pool == null) {
			throw new IllegalArgumentException("thread pool cannot be null");
		}

		this.runner = homeworkRunner;
		this.pool = pool;
	}

	@RequestMapping(path="compute", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<HomeworkResult> compute(ListConfig input) {
		if (input == null || !input.isValid()) {
			// https://tools.ietf.org/html/rfc7231#section-6.5.13
			return new ResponseEntity<>(HttpStatus.UNSUPPORTED_MEDIA_TYPE);
		}

		LOGGER.trace("Received request set '{}', iterate '{}'", input.getSetListSize(), input.getIterateListSize());

		CompletableFuture<HomeworkResult> future = asyncTimeIntersection(input);

		HomeworkResult result;
		try {

			result = future.get(TIMEOUT_SECONDS, TimeUnit.SECONDS);

		} catch (TimeoutException e) {
			LOGGER.info("Timeout reached for set '{}', iterate '{}'", input.getSetListSize(), input.getIterateListSize());
			return new ResponseEntity<>(HttpStatus.REQUEST_TIMEOUT);
		} catch (ExecutionException | InterruptedException e) {
			LOGGER.warn("Internal server error", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		if (result != null) {
			// this actually happens in the unit test when ran from cmd line...
			// from eclipse, does not happen
			LOGGER.trace("Success duration '{}', size '{}'", result.getDuration(), result.getSize());
		}

		return ResponseEntity.ok(result);
	}

	private CompletableFuture<HomeworkResult> asyncTimeIntersection(ListConfig input) {
		return CompletableFuture.supplyAsync(() -> {
			return runner.timeIntersection(input.getSetListSize(), input.getIterateListSize());
		}, pool);
	}
}