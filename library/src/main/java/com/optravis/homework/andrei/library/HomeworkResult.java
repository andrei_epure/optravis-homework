package com.optravis.homework.andrei.library;

import java.time.Duration;
import java.time.Instant;

public class HomeworkResult {
	private final String duration;
	private final int size;

	public HomeworkResult(Instant start, Instant end, int size) {
		if (start == null) {
			throw new IllegalArgumentException("start is null");
		}
		if (end == null) {
			throw new IllegalArgumentException("end is null");
		}
		if (start.isAfter(end)) {
			throw new IllegalArgumentException("start is after end");
		}
		if (size < 0) {
			throw new IllegalArgumentException("Size is negative");
		}

		this.duration = Duration.between(start, end).toString();
		this.size = size;
	}

	public String getDuration() { return duration; }
	public int getSize() { return size; }
}