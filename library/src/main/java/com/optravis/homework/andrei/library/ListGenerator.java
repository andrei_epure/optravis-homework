package com.optravis.homework.andrei.library;

import java.util.List;

public interface ListGenerator {
	List<Integer> generateList(int size);
}