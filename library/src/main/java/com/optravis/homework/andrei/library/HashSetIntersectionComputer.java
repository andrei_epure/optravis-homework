package com.optravis.homework.andrei.library;

import java.util.List;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HashSetIntersectionComputer implements SetIntersectionComputer {
	private final static Logger LOGGER = LoggerFactory.getLogger(HashSetIntersectionComputer.class);

	@Override
	public Set<Integer> computeIntersection(List<Integer> setList, List<Integer> iterateList) {
		if (setList == null) {
			throw new IllegalArgumentException("setList cannot be null");
		}
		if (iterateList == null) {
			throw new IllegalArgumentException("iterateList cannot be null");
		}

		HashSet<Integer> setListNumbers = new HashSet<>(setList);
		HashSet<Integer> result = new HashSet<>();

		LOGGER.trace("Input set list has '{}' elements, '{}' unique; iterate list '{}' elements", setList.size(),
				setListNumbers.size(), iterateList.size());

		for (Integer iterateListNumber : iterateList) {
			if (setListNumbers.contains(iterateListNumber)) {
				result.add(iterateListNumber);
			}
		}

		return result;
	}
}