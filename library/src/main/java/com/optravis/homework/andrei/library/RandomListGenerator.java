package com.optravis.homework.andrei.library;

import java.util.List;
import java.util.LinkedList;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RandomListGenerator implements ListGenerator {
	private final static Logger LOGGER = LoggerFactory.getLogger(RandomListGenerator.class);

	@Override
	public List<Integer> generateList(int size) {
		if (size < 0) {
			throw new IllegalArgumentException("Size cannot be negative");
		}

		LOGGER.trace("Will generate list of '{}' elements", size);

		LinkedList<Integer> result = new LinkedList<>();
		Random generator = new Random();
		for (int i = 0; i < size; i++) {
			result.add(generator.nextInt());
		}

		LOGGER.trace("Finished generating list of '{}' elements", size);

		return result;
	}
}