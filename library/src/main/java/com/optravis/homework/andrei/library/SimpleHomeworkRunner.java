package com.optravis.homework.andrei.library;

import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;

public class SimpleHomeworkRunner implements HomeworkRunner {
	private final static Logger LOGGER = LoggerFactory.getLogger(SimpleHomeworkRunner.class);
	private final ListGenerator listGenerator;
	private final SetIntersectionComputer intersectionComputer;

	public SimpleHomeworkRunner(ListGenerator listGenerator, SetIntersectionComputer intersectionComputer) {
		if (listGenerator == null) {
			throw new IllegalArgumentException("listGenerator cannot be null");
		}
		if (intersectionComputer == null) {
			throw new IllegalArgumentException("intersectionComputer cannot be null");
		}

		this.listGenerator = listGenerator;
		this.intersectionComputer = intersectionComputer;
	}

	@Override
	public HomeworkResult timeIntersection(int setListSize, int iterateListSize) {
		if (setListSize < 0) {
			throw new IllegalArgumentException("setListSize cannot be negative");
		}
		if (iterateListSize < 0) {
			throw new IllegalArgumentException("iterateListSize cannot be negative");
		}

		List<Integer> setList = listGenerator.generateList(setListSize);
		List<Integer> iterateList = listGenerator.generateList(iterateListSize);

		Instant start = Instant.now();
		Set<Integer> result = intersectionComputer.computeIntersection(setList, iterateList); 
		Instant end = Instant.now();

		LOGGER.trace("Started at '{}', ended at '{}', got '{}' elements", start, end, result.size());

		return new HomeworkResult(start, end, result.size());
	}
}