package com.optravis.homework.andrei.library;

import java.util.List;
import java.util.Set;

public interface SetIntersectionComputer {
	Set<Integer> computeIntersection(List<Integer> setList, List<Integer> iterateList);
}