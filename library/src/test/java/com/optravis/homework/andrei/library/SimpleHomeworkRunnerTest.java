package com.optravis.homework.andrei.library;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.times;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.internal.util.collections.Sets;

public class SimpleHomeworkRunnerTest {

	@Test(expected = IllegalArgumentException.class)
	public void runnerWithNullListGeneratorThrows() {
		SetIntersectionComputer setIntersectionComputer = mock(SetIntersectionComputer.class);
		SimpleHomeworkRunner runner = new SimpleHomeworkRunner(null, setIntersectionComputer);
	}

	@Test(expected = IllegalArgumentException.class)
	public void runnerWithNullIntersectionComputerThrows() {
		ListGenerator listGenerator = mock(ListGenerator.class);
		SimpleHomeworkRunner runner = new SimpleHomeworkRunner(listGenerator, null);
	}

	@Test(expected = IllegalArgumentException.class)
	public void timeIntersectionWithNegativeSetListSizeThrows() {
		ListGenerator listGenerator = mock(ListGenerator.class);
		SetIntersectionComputer setIntersectionComputer = mock(SetIntersectionComputer.class);
		SimpleHomeworkRunner runner = new SimpleHomeworkRunner(listGenerator, setIntersectionComputer);

		runner.timeIntersection(-1, 0);
	}

	@Test(expected = IllegalArgumentException.class)
	public void timeIntersectionWithNegativeIterateListSizeThrows() {
		ListGenerator listGenerator = mock(ListGenerator.class);
		SetIntersectionComputer setIntersectionComputer = mock(SetIntersectionComputer.class);
		SimpleHomeworkRunner runner = new SimpleHomeworkRunner(listGenerator, setIntersectionComputer);

		runner.timeIntersection(0, -1);
	}

	@Test
	public void timeIntersectionWithZeroListSizesCallsMethods() {
		// setup
		ListGenerator mockListGenerator = mock(ListGenerator.class);
		SetIntersectionComputer mockIntersectionComputer = mock(SetIntersectionComputer.class);
		when(mockListGenerator.generateList(0)).thenReturn(Arrays.asList());

		Set<Integer> mockResult = Sets.newSet(1, 2, 3);
		when(mockIntersectionComputer.computeIntersection(isA(List.class), isA(List.class))).thenReturn(mockResult);

		// act
		SimpleHomeworkRunner runner = new SimpleHomeworkRunner(mockListGenerator, mockIntersectionComputer);
		HomeworkResult result = runner.timeIntersection(0, 0);

		// verify
		assertNotNull("result is not null", result);
		assertNotNull("result duration is not null", result.getDuration());
		assertEquals("Must return set from setIntersectionComputer", 3, result.getSize());

		verify(mockListGenerator, times(2)).generateList(0);
		verify(mockIntersectionComputer, times(1)).computeIntersection(isA(List.class), isA(List.class));
	}

	@Test
	public void timeIntersectionPassesCorrectListToSetIntersectionComputer() {
		// setup
		ListGenerator mockListGenerator = mock(ListGenerator.class);
		SetIntersectionComputer mockIntersectionComputer = mock(SetIntersectionComputer.class);

		int testSetListNumber = 5;
		int testIteratorListNumber = 1;
		List<Integer> testSetList = Arrays.asList(4, 5, 6);
		List<Integer> testIteratorList = Arrays.asList(1, 2, 3);

		when(mockListGenerator.generateList(testSetListNumber)).thenReturn(testSetList);
		when(mockListGenerator.generateList(testIteratorListNumber)).thenReturn(testIteratorList);
		when(mockIntersectionComputer.computeIntersection(isA(List.class), isA(List.class))).thenReturn(Sets.newSet());

		// act
		SimpleHomeworkRunner runner = new SimpleHomeworkRunner(mockListGenerator, mockIntersectionComputer);
		HomeworkResult result = runner.timeIntersection(testSetListNumber, testIteratorListNumber);

		// verify
		verify(mockIntersectionComputer, times(1)).computeIntersection(ArgumentMatchers.eq(testSetList),
				ArgumentMatchers.eq(testIteratorList));
	}
}