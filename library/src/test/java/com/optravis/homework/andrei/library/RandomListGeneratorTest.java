package com.optravis.homework.andrei.library;

import static org.junit.Assert.assertEquals;

import java.util.List;
import org.junit.Test;

public class RandomListGeneratorTest {

	@Test(expected = IllegalArgumentException.class)
	public void generateListWithNegativeSizeShouldThrow() {
		RandomListGenerator tester = new RandomListGenerator();
		tester.generateList(-1);
	}

	@Test
	public void generateListWithZeroReturnsEmptyList() {
		RandomListGenerator tester = new RandomListGenerator();
		List<Integer> result = tester.generateList(0);

		assertEquals(0, result.size());
	}

	@Test
	public void generateListWithPositiveSizeHasRightSize() {
		RandomListGenerator tester = new RandomListGenerator();
		int size = 10;
		List<Integer> result = tester.generateList(10);

		assertEquals(size, result.size());
	}
}