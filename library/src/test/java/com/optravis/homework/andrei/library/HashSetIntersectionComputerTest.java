package com.optravis.homework.andrei.library;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.junit.Test;

public class HashSetIntersectionComputerTest {

	@Test(expected = IllegalArgumentException.class)
	public void computeIntersectionWithNullSetListThrows() {
		HashSetIntersectionComputer computer = new HashSetIntersectionComputer();
		computer.computeIntersection(null, Arrays.asList());
	}

	@Test(expected = IllegalArgumentException.class)
	public void computeIntersectionWithNullIterateListThrows() {
		HashSetIntersectionComputer computer = new HashSetIntersectionComputer();
		computer.computeIntersection(Arrays.asList(), null);
	}

	@Test
	public void computeIntersectionWithEmptyListsReturnsEmptySet() {
		HashSetIntersectionComputer computer = new HashSetIntersectionComputer();

		Set<Integer> result = computer.computeIntersection(Arrays.asList(), Arrays.asList());

		assertEquals(0, result.size());
	}

	@Test
	public void computeIntersectionWithVoidIntersectionReturnsEmptySet() {
		HashSetIntersectionComputer computer = new HashSetIntersectionComputer();
		List<Integer> setList = Arrays.asList(1, 2, 3);
		List<Integer> iterateList = Arrays.asList(4, 5, 6);

		Set<Integer> result = computer.computeIntersection(setList, iterateList);

		assertEquals(0, result.size());
	}

	@Test
	public void computeIntersectionWithNonVoidIntersetionReturnsIntersection() {
		HashSetIntersectionComputer computer = new HashSetIntersectionComputer();
		int common = 3;
		List<Integer> setList = Arrays.asList(1, 2, common);
		List<Integer> iterateList = Arrays.asList(4, 5, 6, common);

		Set<Integer> result = computer.computeIntersection(setList, iterateList);

		assertEquals(1, result.size());
		assertTrue(result.contains(common));
	}

	@Test
	public void computeIntersectionWithSameListsReturnsIntersection() {
		HashSetIntersectionComputer computer = new HashSetIntersectionComputer();
		List<Integer> setList = Arrays.asList(1, 2, 3);

		Set<Integer> result = computer.computeIntersection(setList, setList);

		assertEquals(3, result.size());
		assertTrue(result.contains(1));
		assertTrue(result.contains(2));
		assertTrue(result.contains(3));
	}
}