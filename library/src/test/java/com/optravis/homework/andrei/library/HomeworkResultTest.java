package com.optravis.homework.andrei.library;

import static org.junit.Assert.assertEquals;

import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import org.junit.Test;

public class HomeworkResultTest {

	@Test(expected = IllegalArgumentException.class)
	public void homeworkResultWithNullStartThrows() {
		HomeworkResult result = new HomeworkResult(null, Instant.MIN, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void homeworkResultWithNullEndThrows() {
		HomeworkResult result = new HomeworkResult(Instant.MIN, null, 1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void homeworkResultWithNegativeSizeThrows() {
		HomeworkResult result = new HomeworkResult(Instant.MIN, Instant.MAX, -1);
	}

	@Test(expected = IllegalArgumentException.class)
	public void homeworkResultWithStartAfterEndThrows() {
		HomeworkResult result = new HomeworkResult(Instant.MAX, Instant.MIN, -1);
	}

	@Test
	public void homeworkResultReturnsCorrectValues() {
		Instant start = Instant.now();
		Instant end = start.plus(2, ChronoUnit.MINUTES);
		String expectedDuration = Duration.between(start, end).toString();
		int expectedSize = 42;
		HomeworkResult result = new HomeworkResult(start, end, expectedSize);

		assertEquals("duration is correct", expectedDuration, result.getDuration());
		assertEquals("size is correct", expectedSize, result.getSize());
	}
}